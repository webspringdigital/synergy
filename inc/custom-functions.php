<?php
/**
 * Custom functions
 *
 * @package synergy
 */

/**
 * Custom Edit Button
 *
 * @param string $output Adds Bootstrap classes.
 */
function custom_edit_post_link( $output ) {

	$output = str_replace( 'class="post-edit-link"', 'class="post-edit-link btn btn-warning btn-sm"', $output );
	return $output;
}
add_filter( 'edit_post_link', 'custom_edit_post_link' );
