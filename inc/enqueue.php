<?php
/**
 * Synergy enqueue scripts
 *
 * @package synergy
 */

/**
 * Set path for manifest
 * @return string
 */
function asset_path( $filename ) {
	$manifest_path = get_template_directory() . '/rev-manifest.json';
	if ( file_exists( $manifest_path ) ) {
		$manifest = json_decode( wpcom_vip_file_get_contents( $manifest_path ), true );
	} else {
		$manifest = [];
	}
	if ( array_key_exists( $filename, $manifest ) ) {
		return $manifest[ $filename ];
	}
	return $filename;
}

if ( ! function_exists( 'synergy_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function synergy_scripts() {

		wp_enqueue_style( 'synergy-styles', get_template_directory_uri() . '/' . asset_path( 'assets/css/style.min.css' ), array(), null );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/assets/js/popper.min.js', array(), false, true );
		wp_enqueue_script( 'synergy-scripts', get_template_directory_uri() . '/' . asset_path( 'assets/js/theme.min.js' ), array(), null, true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'synergy_scripts' ).

add_action( 'wp_enqueue_scripts', 'synergy_scripts' );
