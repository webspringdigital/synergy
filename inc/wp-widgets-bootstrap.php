<?php
/**
 * WP Widgets Bootstrap
 * Extends Widget Output Filters Plugin
 *
 * "Every passing minute is another chance to turn it all around."
 * ~ Vanilla Sky
 *
 * Based on code in the WordPress Widgets Bootstrapped plugin by Bryan Willis
 * @link        https://github.com/Wordpress-Development/wordpress-widgets-bootstrapped
 *
 * @package     wp-widgets-bootstrap
 * @author      Bryan Willis
 * @author      Noel Springer
 * @link        https://bitbucket.org/webspringdigital/wp-widgets-bootstrap
 * @copyright   Bryan Willis
 * @copyright   Copyright (c) 2018 Noel Springer
 * @license     GPLv2.0
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * @wordpress-plugin
 * Plugin Name: WP Widgets Bootstrap
 * Plugin URI:  https://bitbucket.org/webspringdigital/wp-widgets-bootstrap
 * Description: Extends Widget Output Filters to add Bootstrap 4 markup to WordPress widgets. Requires the Widget Output Filters Plugin and theme with Bootstrap 4 support.
 * Version:     1.1.1
 * Author:      Noel Springer
 * Author URI:  https://www.noelspringer.com
 */

/**
 * This plugin does the following on activation:
 * - Filters the HTML markup to support Bootstrap 4 styling. Without Bootstrap enqueued it won't appear to do anything.
 * - Supports - categories, calendar, tag cloud, archives, meta, recent-posts, recent-comments, pages, nav-menu, search
 */

defined( 'WPINC' ) || die;

/**
 * Bootstrap 4 Widget Filters
 */
add_filter( 'widget_output', 'wop_bootstrap_widget_output_filters', 11, 4 );
/**
 * Replaces standard WordPress markup with Bootstrap markup.
 *
 * @param string $widget_output Outputs the filtered markup.
 * @param string $widget_type Identifies widget of specific type e.g. categories.
 * @param string $widget_id Identifies the widget.
 * @param string $sidebar_id Identifies the sidebar by its id.
 * @return widget_output
 */
function wop_bootstrap_widget_output_filters( $widget_output, $widget_type, $widget_id, $sidebar_id ) {
	switch ( $widget_type ) {
		case 'categories':
			$widget_output = str_replace( '<h3 class="widget-title">', '<div class="card"><div class="bg-light"><h3 class="card-title widget-title pt-3 pl-3">', $widget_output );
			$widget_output = str_replace( '</h3>', '</h3></div>', $widget_output );
			$widget_output = str_replace( '<ul>', '<ul class="list-group list-group-flush">', $widget_output );
			$widget_output = str_replace( '<li class="cat-item-none">', '<li class="cat-item-none list-group-item">', $widget_output );
			$widget_output = str_replace( '<li class="cat-item cat-item-', '<li class="list-group-item list-group-item-action cat-item cat-item-', $widget_output );
			$widget_output = str_replace( '<a', '<i class="far fa-folder text-muted"></i> <a', $widget_output );
			$widget_output = str_replace( '(', '<span class="cat-item-count badge badge-secondary float-right"> ', $widget_output );
			$widget_output = str_replace( ')', ' </span>', $widget_output );
			$widget_output = str_replace( '</ul>', '</ul></div>', $widget_output );
			break;
		case 'calendar':
			$widget_output = str_replace( '<table id="wp-calendar', '<table class="table" id="wp-calendar', $widget_output );
			break;
		case 'tag_cloud':
			$widget_output = str_replace( '<h3 class="widget-title">', '<div class="card"><div class="bg-light border-bottom"><h3 class="card-title widget-title pt-3 pl-3">', $widget_output );
			$widget_output = str_replace( '</h3>', '</h3></div>', $widget_output );
			$widget_output = str_replace( 'class="tagcloud"', 'class="card-body tagcloud"', $widget_output );
			$widget_output = str_replace( 'class="tag-cloud-link', 'class="badge badge-secondary tag-cloud-link', $widget_output );
			$widget_output = str_replace( '</a>', '</a> ', $widget_output );
			$widget_output = str_replace( '</ul>', '</ul></div>', $widget_output );
			break;
		case 'archives':
			$widget_output = str_replace( '<h3 class="widget-title">', '<div class="card"><div class="bg-light"><h3 class="card-title widget-title pt-3 pl-3">', $widget_output );
			$widget_output = str_replace( '</h3>', '</h3></div>', $widget_output );
			$widget_output = str_replace( '<ul>', '<ul class="list-group list-group-flush">', $widget_output );
			$widget_output = str_replace( '<li>', '<li class="list-group-item list-group-item-action">', $widget_output );
			$widget_output = str_replace( '<a', '<i class="fas fa-archive text-muted"></i> <a', $widget_output );
			$widget_output = str_replace( '(', '<span class="cat-item-count badge badge-secondary float-right"> ', $widget_output );
			$widget_output = str_replace( ')', ' </span>', $widget_output );
			$widget_output = str_replace( '</ul>', '</ul></div>', $widget_output );
			break;
		case 'meta':
			$widget_output = str_replace( '<h3 class="widget-title">', '<div class="card"><div class="bg-light"><h3 class="card-title widget-title pt-3 pl-3">', $widget_output );
			$widget_output = str_replace( '</h3>', '</h3></div>', $widget_output );
			$widget_output = str_replace( '<ul>', '<ul class="list-group list-group-flush">', $widget_output );
			$widget_output = str_replace( '<li>', '<li class="list-group-item list-group-item-action">', $widget_output );
			$widget_output = str_replace( '</ul>', '</ul></div>', $widget_output );
			break;
		case 'recent-posts':
			$widget_output = str_replace( '<h3 class="widget-title">', '<div class="card"><div class="bg-light"><h3 class="card-title widget-title pt-3 pl-3">', $widget_output );
			$widget_output = str_replace( '</h3>', '</h3></div>', $widget_output );
			$widget_output = str_replace( '<ul>', '<ul class="list-group list-group-flush">', $widget_output );
			$widget_output = str_replace( '<li>', '<li class="list-group-item list-group-item-action">', $widget_output );
			$widget_output = str_replace( '<a', '<i class="far fa-file-alt text-muted"></i> <a', $widget_output );
			$widget_output = str_replace( 'class="post-date"', 'class="post-date badge badge-secondary text-light small float-right"', $widget_output );
			$widget_output = str_replace( '</ul>', '</ul></div>', $widget_output );
			break;
		case 'recent-comments':
			$widget_output = str_replace( '<h3 class="widget-title">', '<div class="card"><div class="bg-light"><h3 class="card-title widget-title pt-3 pl-3">', $widget_output );
			$widget_output = str_replace( '</h3>', '</h3></div>', $widget_output );
			$widget_output = str_replace( '<ul id="recentcomments">', '<ul id="recentcomments" class="list-group list-group-flush">', $widget_output );
			$widget_output = str_replace( '<li class="recentcomments">', '<li class="recentcomments list-group-item">', $widget_output );
			$widget_output = str_replace( '<span class="comment-author-link">', '<i class="fas fa-user text-muted"></i> <span class="comment-author-link">', $widget_output );
			$widget_output = str_replace( '</ul>', '</ul></div>', $widget_output );
			break;
		case 'pages':
			$widget_output = str_replace( '<h3 class="widget-title">', '<div class="card"><div class="bg-light"><h3 class="card-title widget-title pt-3 pl-3">', $widget_output );
			$widget_output = str_replace( '</h3>', '</h3></div>', $widget_output );
			$widget_output = str_replace( '<ul>', '<ul class="list-group list-group-flush">', $widget_output );
			$widget_output = str_replace( '<li class="page_item', '<li class="list-group-item list-group-item-action', $widget_output );
			$widget_output = str_replace( '<a', '<i class="far fa-file-alt text-muted"></i> <a', $widget_output );
			$widget_output = str_replace( '</ul>', '</ul></div>', $widget_output );
			break;
		case 'nav_menu':
			$widget_output = str_replace( ' class="menu"', ' class="menu nav flex-column nav-pills"', $widget_output );
			$widget_output = str_replace( ' class="menu-item', ' class="nav-item', $widget_output );
			break;
		case 'woocommerce_product_categories':
			$widget_output = str_replace( '<h3 class="widget-title">', '<div class="card"><div class="bg-light"><h3 class="card-title widget-title pt-3 pl-3">', $widget_output );
			$widget_output = str_replace( '</h3>', '</h3></div>', $widget_output );
			$widget_output = str_replace( '<ul class="product-categories">', '<ul class="product-categories list-group list-group-flush">', $widget_output );
			$widget_output = str_replace( '<li class="cat-item-none">', '<li class="cat-item-none list-group-item">', $widget_output );
			$widget_output = str_replace( '<li class="cat-item', '<li class="list-group-item list-group-item-action cat-item', $widget_output );
			$widget_output = str_replace( '<a', '<i class="far fa-folder text-muted"></i> <a', $widget_output );
			$widget_output = str_replace( '(', '<span class="cat-item-count badge badge-secondary float-right"> ', $widget_output );
			$widget_output = str_replace( ')', ' </span>', $widget_output );
			$widget_output = str_replace( '</ul>', '</ul></div>', $widget_output );
			break;
		default:
			$widget_output = $widget_output;
	}
	return $widget_output;
}
