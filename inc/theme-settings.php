<?php
/**
 * Check and setup theme's default settings
 *
 * @package synergy
 */

if ( ! function_exists( 'synergy_setup_theme_default_settings' ) ) {
	/**
	 * Check and setup theme's default setting
	 *
	 * @return void
	 */
	function synergy_setup_theme_default_settings() {

		// check if settings are set, if not set defaults.
		// Caution: DO NOT check existence using === always check with == .
		// Latest blog posts style.
		$synergy_posts_index_style = get_theme_mod( 'synergy_posts_index_style' );
		if ( '' === $synergy_posts_index_style ) {
			set_theme_mod( 'synergy_posts_index_style', 'default' );
		}

		// Sidebar position.
		$synergy_sidebar_position = get_theme_mod( 'synergy_sidebar_position' );
		if ( '' === $synergy_sidebar_position ) {
			set_theme_mod( 'synergy_sidebar_position', 'right' );
		}

		// Container width.
		$synergy_container_type = get_theme_mod( 'synergy_container_type' );
		if ( '' === $synergy_container_type ) {
			set_theme_mod( 'synergy_container_type', 'container' );
		}
	}
}
