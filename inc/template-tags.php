<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package synergy
 */

if ( ! function_exists( 'synergy_posted_on' ) ) {
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function synergy_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s"> (%4$s) </time>';
		}
		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);
		$posted_on   = sprintf(
			/* translators: %s: post date */
			'<i class="fas fa-calendar-alt" aria-hidden="true"></i>' . _x( ' %s ', 'post date', 'synergy' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);
		$byline      = sprintf(
			/* translators: %s: post author */
			'<i class="fas fa-user" aria-hidden="true"></i>' . _x( ' %s ', 'post author', 'synergy' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);
		echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'synergy' ) );
			if ( $categories_list && synergy_categorized_blog() ) {
				printf( '<i class="fas fa-folder" aria-hidden="true"></i><span class="cat-links">' . esc_html__( ' %1$s ', 'synergy' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}
			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html__( ', ', 'synergy' ) );
			if ( $tags_list ) {
				printf( '<i class="fas fa-tag" aria-hidden="true"></i><span class="tags-links">' . esc_html__( ' %1$s ', 'synergy' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}
		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<i class="fas fa-comments" aria-hidden="true"></i> <span class="comments-link">';
			comments_popup_link( esc_html__( 'Leave a comment', 'synergy' ), esc_html__( '1 Comment', 'synergy' ), esc_html__( '% Comments', 'synergy' ) );
			echo '</span>';
		}
	}
}

if ( ! function_exists( 'synergy_entry_footer' ) ) {
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function synergy_entry_footer() {
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				esc_html__( 'Edit %s', 'synergy' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
}

if ( ! function_exists( 'synergy_categorized_blog' ) ) {
	/**
	 * Returns true if a blog has more than 1 category.
	 *
	 * @return bool
	 */
	function synergy_categorized_blog() {
		if ( false === ( $all_the_cool_cats = get_transient( 'synergy_categories' ) ) ) {
			// Create an array of all the categories that are attached to posts.
			$all_the_cool_cats = get_categories(
				array(
					'fields'     => 'ids',
					'hide_empty' => 1,
					// We only need to know if there is more than one category.
					'number'     => 2,
				)
			);
			// Count the number of categories that are attached to the posts.
			$all_the_cool_cats = count( $all_the_cool_cats );
			set_transient( 'synergy_categories', $all_the_cool_cats );
		}
		if ( $all_the_cool_cats > 1 ) {
			// This blog has more than 1 category so components_categorized_blog should return true.
			return true;
		} else {
			// This blog has only 1 category so components_categorized_blog should return false.
			return false;
		}
	}
}

add_action( 'edit_category', 'synergy_category_transient_flusher' );
add_action( 'save_post', 'synergy_category_transient_flusher' );

if ( ! function_exists( 'synergy_category_transient_flusher' ) ) {
	/**
	 * Flush out the transients used in synergy_categorized_blog.
	 */
	function synergy_category_transient_flusher() {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}
		// Like, beat it. Dig?
		delete_transient( 'synergy_categories' );
	}
}
