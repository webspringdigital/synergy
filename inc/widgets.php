<?php
/**
 * Declaring widgets
 *
 * @package synergy
 */

/**
 * Count number of widgets in a sidebar
 * Used to add classes to widget areas so widgets can be displayed one, two, three or four per row
 */
if ( ! function_exists( 'synergy_slbd_count_widgets' ) ) {
	/**
	 * Count number of widgets in a sidebar
	 * Used to add classes to widget areas so widgets can be displayed one, two, three or four per row
	 *
	 * @param string $sidebar_id sidebar id.
	 */
	function synergy_slbd_count_widgets( $sidebar_id ) {
		$sidebars_widgets_count = get_option( 'sidebars_widgets', array() );
		if ( isset( $sidebars_widgets_count[ $sidebar_id ] ) ) :
			$widget_count   = count( $sidebars_widgets_count[ $sidebar_id ] );
			$widget_classes = 'widget-count-' . count( $sidebars_widgets_count[ $sidebar_id ] );
			if ( 0 === $widget_count % 4 || 6 < $widget_count ) :
				// Four widgets per row if there are exactly four or more than six.
				$widget_classes .= ' col-md-3';
			elseif ( 6 === $widget_count ) :
				// If two widgets are published.
				$widget_classes .= ' col-md-2';
			elseif ( 3 <= $widget_count ) :
				// Three widgets per row if there's three or more widgets.
				$widget_classes .= ' col-md-4';
			elseif ( 2 === $widget_count ) :
				// If two widgets are published.
				$widget_classes .= ' col-md-6';
			elseif ( 1 === $widget_count ) :
				// If just on widget is active.
				$widget_classes .= ' col-md-12';
			endif;
			return $widget_classes;
		endif;
	}
}

add_action( 'widgets_init', 'synergy_widgets_init' );

if ( ! function_exists( 'synergy_widgets_init' ) ) {
	/**
	 * Initializes themes widgets.
	 */
	function synergy_widgets_init() {
		register_sidebar(
			array(
				'name'          => __( 'Right Sidebar', 'synergy' ),
				'id'            => 'right-sidebar',
				'description'   => 'Right sidebar widget area',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);

		register_sidebar(
			array(
				'name'          => __( 'Left Sidebar', 'synergy' ),
				'id'            => 'left-sidebar',
				'description'   => 'Left sidebar widget area',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);

		register_sidebar(
			array(
				'name'          => __( 'Hero Slider', 'synergy' ),
				'id'            => 'hero',
				'description'   => 'Hero slider area. Place two or more widgets here and they will slide!',
				'before_widget' => '<div class="carousel-item">',
				'after_widget'  => '</div>',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => __( 'Hero Canvas', 'synergy' ),
				'id'            => 'herocanvas',
				'description'   => 'Full size canvas hero area for Bootstrap and other custom HTML markup',
				'before_widget' => '',
				'after_widget'  => '',
				'before_title'  => '',
				'after_title'   => '',
			)
		);

		register_sidebar(
			array(
				'name'          => __( 'Top Full', 'synergy' ),
				'id'            => 'statichero',
				'description'   => 'Full top widget with dynamic grid',
				'before_widget' => '<div id="%1$s" class="static-hero-widget %2$s ' . synergy_slbd_count_widgets( 'statichero' ) . '">',
				'after_widget'  => '</div><!-- .static-hero-widget -->',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);

		register_sidebar(
			array(
				'name'          => __( 'Bottom Full', 'synergy' ),
				'id'            => 'footerfull',
				'description'   => 'Full bottom widget with dynamic grid',
				'before_widget' => '<div id="%1$s" class="footer-widget %2$s ' . synergy_slbd_count_widgets( 'footerfull' ) . '">',
				'after_widget'  => '</div><!-- .footer-widget -->',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			)
		);

		register_sidebar( array(
			'name'          => __( 'Content Top Full', 'synergy' ),
			'id'            => 'content-top-full',
			'description'   => 'Widget area below navbar and above main content.',
			'before_widget' => '<div id="content-top-widget" class="content-top-widget d-none d-sm-block">',
			'after_widget'  => '</div><!-- /.content-top -->',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Content Bottom', 'synergy' ),
			'id'            => 'content-bottom',
			'description'   => 'Widget area below main content',
			'before_widget' => '<div id="%1$s" class="content-bottom-widget %2s ' . synergy_slbd_count_widgets( 'content-bottom' ) . '">',
			'after_widget'  => '</div><!-- /.content-bottom-widget -->',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Content Bottom Full', 'synergy' ),
			'id'            => 'content-bottom-full',
			'description'   => 'Widget area below main content and above footer',
			'before_widget' => '<div id="%1$s" class="content-bottom-full-widget %2$s ' . synergy_slbd_count_widgets( 'content-bottom-full' ) . '">',
			'after_widget'  => '</div><!-- /.content-bottom-full-widget -->',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}
} // endif function_exists( 'synergy_widgets_init' ).
