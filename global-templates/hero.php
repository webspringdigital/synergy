<?php
/**
 * Hero setup.
 *
 * @package synergy
 */

?>

<?php if ( is_active_sidebar( 'hero' ) || is_active_sidebar( 'statichero' ) || is_active_sidebar( 'herocanvas' ) ) : ?>

	<div class="wrapper" id="wrapper-hero">

		<?php get_template_part( 'global-templates/sidebar', 'hero' ); ?>

		<?php get_template_part( 'global-templates/sidebar', 'herocanvas' ); ?>

		<?php get_template_part( 'global-templates/sidebar', 'statichero' ); ?>

	</div>

<?php endif; ?>

