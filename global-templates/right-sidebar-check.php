<?php
/**
 * Right sidebar check.
 *
 * @package synergy
 */

?>

</div><!-- #closing the primary container from /global-templates/left-sidebar-check.php -->

<?php $sidebar_pos = get_theme_mod( 'synergy_sidebar_position' ); ?>

<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

	<?php get_template_part( 'global-templates/sidebar', 'right' ); ?>

<?php endif; ?>

