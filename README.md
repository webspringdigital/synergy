# Synergy WordPress Theme

**See: [Demo - coming soon!](#)**

**Website:** [Coming soon!](#)

**Child Theme Project:** [https://bitbucket.org/webspring/synergy-child](https://bitbucket.org/webspringdigital/synergy-child)

## About

Synergy is based on [UnderStrap](https://understrap.com/understrap/) which in turn is based on a solid foundation of [Underscores (_s)](https://underscores.me/) and [Bootstrap](https://getbootstrap.com/). You can use Synergy as a starter theme or you can use it as a parent theme and create your own child theme for Synergy.

## License

Synergy WordPress Theme, Copyright 2018 Noel Springer  
Synergy is distributed under the terms of the GNU GPL version 2.0
[http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html](http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

See **Licenses & Credits** below for UnderStrap and other licenses.

## Changelog

See [changelog](CHANGELOG.md)

## Basic Features

- Combines Underscore’s PHP/JS files and Bootstrap’s HTML/CSS/JS.
- Comes with Bootstrap (v4) Sass source files and additional .scss files with which to add your own creativity. Incorporate your own custom styles and customize the Bootstrap variables where required.
- Uses a single minified CSS file for all the basic styling.
- [Font Awesome](https://fontawesome.com/) icon fonts integration (v5.0.13)
- Jetpack ready.
- WooCommerce support.
- [Child Theme](https://bitbucket.org/webspring/synergy-child) ready.
- Translation ready.

## Starter Theme + Frontend Toolkit

The _s theme is a great starting point from which to develop a WordPress theme. It covers all the basics and it's up to you to add your own design. Bootstrap provides a well-known, supported frontend toolkit so you can more easiliy create your own design.

## CSS and Sass Files

Some basics about the Sass and CSS files that come with Synergy:

- The `/style.css` file in the theme's root is only to identify the theme inside of the WordPress dashboard. The file is not enqueued by the theme and does not include any styles.
- The `/css/style.css` file and its minified version `/css/style.min.css` provide all styles. These two files are built from ten SCSS sets and a variables file:

**`/assets/scss/style.scss`**

```css
@import "overrides";  // 1. Add variables to overwrite Bootstrap or Synergy variables here.
@import "../vendor/bootstrap/scss/bootstrap";// 2. Loads Bootstrap 4
@import "theme/synergy"; // 3. Adds some necessary WordPress styles and styles for integrating Bootstrap with Underscores.
@import "theme/woocommerce"; // 4. Fixes for Woocommerce display. Uncomment if not using Woocommerce. 
//Optional files - If you don't use the corresponding scripts/fonts comment them out
@import "../vendor/fontawesome/scss/fontawesome"; // 5. Font Awesome icon fonts.
@import "../vendor/fontawesome/scss/fa-brands"; // 6. Font Awesome brand icon fonts.
@import "../vendor/fontawesome/scss/fa-regular"; // 7. Font Awesome regular icon fonts.
@import "../vendor/fontawesome/scss/fa-solid"; // 8. Font Awesome solid icon fonts.
@import "../vendor/underscores/sass/media/galleries"; // 9. Underscores media styles.

// Any additional imported files
// @import "theme/contact-form7/contact-form7"; // Contact Form 7 - Bootstrap 4 support. Uncomment if using Contact Form 7.
@import "custom"; // 10. This is where you can add your own design.

```

- Don’t edit the no. 2-9 files/filesets or you won’t be able to update your theme without overwriting your own work!
- Your design:
  - Add your styles to the `/assets/scss/_custom.scss` file
  - And your variables to the `/assets/scss/_overrides.scss`
  - Or add other .scss files into the `assets/scss` directory and `@import` them into `/assets/scss/_style.scss`

## Installation

### Classic install

- Download or clone the synergy folder from [Bitbucket](git@bitbucket.org:webspringdigital/synergy.git)
- Extract the `synergy` directory if you downloaded the `.zip` file.
- Upload it into your WordPress installation subfolder: `/wp-content/themes/` or if you are using Bedrock `/web/app/themes/`
- Login to your WordPress dashboard.
- Go to Appearance → Themes.
- Activate the Synergy theme.

## Developing With npm, Gulp and SASS and [Browser Sync][1]

### Installing Dependencies

- Make sure you have installed Node.js and Browser-Sync (optional) on your computer globally
- Then open your terminal and browse to the root of your Synergy directory e.g. `/wp-content/themes/synergy`
- Run: `$ npm install`

### Running

To work with and compile your Sass files on the fly start:

- `$ gulp watch`

Or to run with Browser-Sync:

- First change the browser-sync options to reflect your environment in the file `/gulpconfig.json` in the beginning of the file:

```javascript
{
    "browserSyncOptions" : {
        "proxy": "localhost/theme_test/", // <----- CHANGE HERE
        "notify": false
    },
    ...
};
```

- then run: `$ gulp watch-bs`

## How to Use the Built-in Widget Slider

The front-page slider is widget driven. Simply add more than one widget to widget position “Hero”.

- Click on Appearance → Widgets.
- Add two, or more, widgets of any kind to widget area “Hero”.
- That’s it.

## RTL styles

Add a new file to the themes root folder called rtl.css. Add all alignments to this file according to this description:
[https://codex.wordpress.org/Right_to_Left_Language_Support](https://codex.wordpress.org/Right_to_Left_Language_Support)

## Page Templates

### Blank Template

The `blank.php` template is useful when working with various page builders and can be used as a starting blank canvas.

### Empty Template

The `empty.php` template displays a header and a footer only. A good starting point for landing pages.

### Full Width Template

The `fullwidthpage.php` template has full width layout without a sidebar.

## Footnotes

[1] Visit [https://browsersync.io](https://browsersync.io) for more information on Browser Sync

## Licenses & Credits

- UnderStrap WordPress Theme: [https://github.com/understrap/understrap/blob/master/LICENSE.md](https://github.com/understrap/understrap/blob/master/LICENSE.md)
  - Copyright 2013-2017 Holger Koenemann
- Underscores (_s): [https://github.com/Automattic/_s/blob/master/LICENSE](https://github.com/Automattic/_s/blob/master/LICENSE)
- Font Awesome: [https://fontawesome.com/license](https://fontawesome.com/license) (Font: SIL OFL 1.1, CSS: MIT License)
- Bootstrap: [https://getbootstrap.com](https://getbootstrap.com) | [https://github.com/twbs/bootstrap/blob/master/LICENSE](https://github.com/twbs/bootstrap/blob/master/LICENSE) (Code licensed under MIT documentation under CC BY 3.0.)
- jQuery: [https://jquery.org](https://jquery.org) | (Code licensed under MIT)
- WP Bootstrap Navwalker by Edward M`/wp-content/themes/`cIntyre: [https://github.com/twittem/wp-bootstrap-navwalker](https://github.com/twittem/wp-bootstrap-navwalker) | GNU GPL
- Bootstrap Gallery Script based on Roots Sage Gallery: [https://github.com/roots/sage/blob/5b9786b8ceecfe717db55666efe5bcf0c9e1801c/lib/gallery.php](https://github.com/roots/sage/blob/5b9786b8ceecfe717db55666efe5bcf0c9e1801c/lib/gallery.php)
