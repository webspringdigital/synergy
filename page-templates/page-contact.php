<?php
/**
 * Template name: Contact Page
 *
 * Template for displaying Contact page with in-built contact form.
 *
 * @package synergy
 */
$nonce_key = '_contactnonce';

if ( isset( $_POST['submitted'] ) ) {
	if ( empty( $_POST[ $nonce_key ] ) || ! wp_verify_nonce( $_POST[ $nonce_key ], basename( __FILE__ ) ) ) {
		return;
	}

	if ( trim( $_POST['contactName'] ) === '' ) {
		$name_error = true;
		$has_error  = true;
	} else {
		$name = trim( $_POST['contactName'] );
	}

	if ( trim( $_POST['email'] ) === '' ) {
		$email_error = true;
		$has_error   = true;
	} elseif ( ! preg_match( '/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i', trim( $_POST['email'] ) ) ) {
		$email_error = true;
		$has_error   = true;
	} else {
		$email = trim( $_POST['email'] );
	}

	if ( trim( $_POST['comments'] ) === '' ) {
		$comment_error = true;
		$has_error     = true;
	} else {
		if ( function_exists( 'stripslashes' ) ) {
			$comments = stripslashes( trim( $_POST['comments'] ) );
		} else {
			$comments = trim( $_POST['comments'] );
		}
	}

	if ( ! isset( $has_error ) ) {
		$email_to = get_option( 'admin_email' );
		if ( ! isset( $mail_to ) || ( '' === $email_to ) ) {
			$email_to = get_option( 'admin_email' );
		}
		$subject  = '[' . esc_attr( get_bloginfo( 'name' ) ) . ']' . __( ' Enquiry from ', 'synergy' ) . $name;
		$body     = __( 'Name: ', 'synergy' ) . $name . "\n";
		$body    .= __( 'Email: ', 'synergy' ) . $email . "\r\r\n";
		$body    .= __( 'Comments: ', 'synergy' ) . $comments;
		$headers  = __( 'From: ', 'synergy' ) . $name . ' <' . $email_to . '>' . "\r\n";
		$headers .= __( 'Reply-To:', 'synergy' ) . $name . '<' . $email . '>';

		wp_mail( $email_to, $subject, $body, $headers );
		$email_sent = true;
	}
}

get_header();

$container = get_theme_mod( 'synergy_container_type' );

?>

<div class="wrapper" id="contact-page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php
				while ( have_posts() ) :
					the_post();
				?>

				<?php get_template_part( 'loop-templates/content', 'page' ); ?>

				<!-- Contact Form -->
					<div class="row justify-content-center">
						<div class="col-md-8">
					<?php if ( isset( $email_sent ) && true === $email_sent ) { ?>
								<div class="alert alert-success" role="alert">
									<p><?php esc_html_e( 'Thanks, your email was sent successfully.', 'synergy' ); ?></p>
								</div>
							<?php } else { ?>

								<?php if ( isset( $has_error ) || isset( $captcha_error ) ) { ?>
									<div class="alert alert-danger alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									<strong><?php esc_html_e( 'Error!', 'synergy' ); ?></strong> <?php esc_html_e( 'Please try again.', 'synergy' ); ?>
									</div>
								<?php } ?>

								<form action="<?php esc_url( the_permalink() ); ?>" id="contactForm" method="post">
									<?php wp_nonce_field( basename( __FILE__ ), $nonce_key ); ?>
									<div class="form-group
									<?php
									if ( isset( $name_error ) ) {
										echo 'has-error has-feedback';
									}
									?>
									">
											<label class="control-label" for="contactName"><?php esc_html_e( 'Name', 'synergy' ); ?></label>
											<input class="form-control" type="text" name="contactName" id="contactName" value="" />
											<?php if ( isset( $name_error ) ) { ?>
												<span class="form-control-feedback"></span>
											<?php } ?>

									</div>
									<div class="form-group
										<?php
										if ( isset( $email_error ) ) {
											echo 'has-error has-feedback';
										}
										?>
										">
											<label class="control-label" for="email">
											<?php esc_html_e( 'Email', 'synergy' ); ?></label>

											<input class="form-control" type="text" name="email" id="email" value="" />
											<?php if ( isset( $email_error ) ) { ?>
												<span class="form-control-feedback"></span>
											<?php } ?>

									</div>
										<div class="form-group
										<?php
										if ( isset( $comment_error ) ) {
											echo 'has-error has-feedback';
										}
										?>
										">
											<label class="control-label" for="commentsText">
											<?php esc_html_e( 'Message', 'synergy' ); ?></label>

											<textarea class="form-control" name="comments" id="commentsText" rows="10" cols="20"></textarea>
												<?php if ( isset( $comment_error ) ) { ?>
												<span class="form-control-feedback"></span>
											<?php } ?>

									</div>
									<div class="form-actions">
											<button type="submit" class="btn btn-primary"><?php esc_html_e( 'Send Email', 'synergy' ); ?></button>
											<input type="hidden" name="submitted" id="submitted" value="true" />
									</div>
								</form>

						<?php } ?>
						</div><!-- /.col-md-8 -->
					</div><!-- /.row -->

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

		<!-- Do the right sidebar check -->
		<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

	</div><!-- .row -->

</div><!-- Container end -->

</div><!-- Wrapper end -->

<?php get_footer(); ?>
